package com.example.CarService.dto;

import com.example.CarService.model.Service;
import com.example.CarService.model.enums.ServiceStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServiceDTO {

    private Long idService;
    private Date serviceDate;
    private String description;
    private ServiceStatus serviceStatus;

    public ServiceDTO(Service service){
        this(service.getIdService(), service.getServiceDate(), service.getDescription(), service.getServiceStatus());
    }

}
