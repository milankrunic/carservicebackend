package com.example.CarService.dto;

import com.example.CarService.model.Authority;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthorityDTO {

    private Long idAuthority;
    private String nameAuthority;

    public AuthorityDTO(Authority authority) {
        this(authority.getIdAuthority(),authority.getNameAuthority());
    }

}
