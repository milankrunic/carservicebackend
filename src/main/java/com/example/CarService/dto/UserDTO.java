package com.example.CarService.dto;

import com.example.CarService.model.User;
import com.example.CarService.model.UserAuthority;
import com.example.CarService.model.enums.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO {

    private Long idUser;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String address;
    private String phoneNumber;
    private String username;
    private String password;
    private List<AuthorityDTO> authorities = new ArrayList<>();

    public UserDTO(Long idUser, String firstName, String lastName, Gender gender, String address, String phoneNumber, String username,
                       String password, List<UserAuthority> authorities) {
        super();
        this.idUser = idUser;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.username = username;
        this.password = password;
        for (UserAuthority ua : authorities) {
            this.authorities.add(new AuthorityDTO(ua.getAuthority()));
        }
    }

    public UserDTO(User user) {
        this(user.getIdUser(), user.getFirstName(), user.getLastName(), user.getGender(), user.getAddress(), user.getPhoneNumber(),
                user.getUsername(), user.getPassword(), user.getUserAuthorities());
    }

}
