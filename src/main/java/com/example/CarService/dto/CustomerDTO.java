package com.example.CarService.dto;

import com.example.CarService.model.Customer;
import com.example.CarService.model.User;
import com.example.CarService.model.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

    private Long idCustomer;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String address;
    private String phoneNumber;
    private String username;
    private String password;
    private double winningPoints;

    public CustomerDTO(Customer cu){
        this(cu.getUser().getIdUser(), cu.getUser().getFirstName(), cu.getUser().getLastName(), cu.getUser().getGender(),
                cu.getUser().getAddress(), cu.getUser().getPhoneNumber(), cu.getUser().getUsername(),
                cu.getUser().getPassword(), cu.getWinningPoints());
    }
}
