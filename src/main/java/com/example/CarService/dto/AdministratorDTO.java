package com.example.CarService.dto;

import com.example.CarService.model.Administrator;
import com.example.CarService.model.User;
import com.example.CarService.model.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdministratorDTO {

    private Long idAdministrator;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String address;
    private String phoneNumber;
    private String username;
    private String password;
    private double salary;

    public AdministratorDTO(Administrator ad){
        this(ad.getUser().getIdUser(), ad.getUser().getFirstName(), ad.getUser().getLastName(), ad.getUser().getGender(),
                ad.getUser().getAddress(), ad.getUser().getPhoneNumber(), ad.getUser().getUsername(),
                ad.getUser().getPassword(), ad.getSalary());
    }

}
