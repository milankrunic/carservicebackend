package com.example.CarService.dto;

import com.example.CarService.model.Repairer;
import com.example.CarService.model.Service;
import com.example.CarService.model.User;
import com.example.CarService.model.enums.Gender;
import com.example.CarService.model.enums.Specialization;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RepairerDTO {

    private Long idRepairer;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String address;
    private String phoneNumber;
    private String username;
    private String password;
    private double salary;
    private Specialization specialization;

    public RepairerDTO(Repairer re){
        this(re.getUser().getIdUser(), re.getUser().getFirstName(), re.getUser().getLastName(), re.getUser().getGender(),
                re.getUser().getAddress(), re.getUser().getPhoneNumber(), re.getUser().getUsername(),
                re.getUser().getPassword(), re.getSalary(), re.getSpecialization());
    }

}
