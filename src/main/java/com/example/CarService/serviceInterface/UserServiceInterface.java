package com.example.CarService.serviceInterface;

import com.example.CarService.dto.UserDTO;
import com.example.CarService.model.User;

import java.util.List;

public interface UserServiceInterface {

    public User findOneByUsername(String username);
    public List<UserDTO> findAll();
    public UserDTO findOne(Long id);
    public User save(User user);
    public void remove(Long id);

}
