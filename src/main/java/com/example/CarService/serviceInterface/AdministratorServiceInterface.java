package com.example.CarService.serviceInterface;

import com.example.CarService.dto.AdministratorDTO;

import java.util.List;

public interface AdministratorServiceInterface {

    public List<AdministratorDTO> findAll();
    public AdministratorDTO save(AdministratorDTO administratorDTO);

}
