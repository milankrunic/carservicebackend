package com.example.CarService.serviceInterface;

import com.example.CarService.dto.ServiceDTO;

import java.util.List;

public interface ServiceServiceInterface {

    public List<ServiceDTO> findAll();
    public ServiceDTO findOne(Long id);
    public ServiceDTO save(ServiceDTO serviceDTO);
    public void remove(Long id);

}
