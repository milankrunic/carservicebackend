package com.example.CarService.serviceInterface;

import com.example.CarService.dto.CustomerDTO;

import java.util.List;

public interface CustomerServiceInterface {

    public List<CustomerDTO> findAll();
    public CustomerDTO save(CustomerDTO customerDTO);

}
