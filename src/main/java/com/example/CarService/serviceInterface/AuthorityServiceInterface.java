package com.example.CarService.serviceInterface;

import com.example.CarService.model.Authority;

public interface AuthorityServiceInterface {

    public Authority findByNameAuthority(String nameAuthority);

}
