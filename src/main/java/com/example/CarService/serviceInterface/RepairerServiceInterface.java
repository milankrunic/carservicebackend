package com.example.CarService.serviceInterface;

import com.example.CarService.dto.RepairerDTO;

import java.util.List;

public interface RepairerServiceInterface {

    public List<RepairerDTO> findAll();
    public RepairerDTO save(RepairerDTO repairerDTO);

}
