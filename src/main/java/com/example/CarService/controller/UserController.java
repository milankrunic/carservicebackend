package com.example.CarService.controller;

import com.example.CarService.dto.*;
import com.example.CarService.model.*;
import com.example.CarService.security.TokenUtils;
import com.example.CarService.serviceInterface.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "api/user")
public class UserController {
    @Autowired
    UserServiceInterface userServiceInterface;

    @Autowired
    AdministratorServiceInterface administratorServiceInterface;

    @Autowired
    RepairerServiceInterface repairerServiceInterface;

    @Autowired
    CustomerServiceInterface customerServiceInterface;

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    TokenUtils tokenUtils;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<List<UserDTO>> getUsers(){
        return ResponseEntity.ok().body(userServiceInterface.findAll());
    }

    @GetMapping("/all_administrators")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<List<AdministratorDTO>> getAdministrators(){
        return ResponseEntity.ok().body(administratorServiceInterface.findAll());
    }

    @GetMapping("/all_repairers")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<List<RepairerDTO>> getRepairers(){
        return ResponseEntity.ok().body(repairerServiceInterface.findAll());
    }

    @GetMapping("/all_customers")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<List<CustomerDTO>> getCustomers(){
        return ResponseEntity.ok().body(customerServiceInterface.findAll());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/login")
    public ResponseEntity<JwtDTO> createAuthenticationToken(@RequestBody LoginDTO loginDTO) {

        System.out.println("LOGIN STARTS ===================================================");

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(),
                        loginDTO.getPassword()));

        // Insert the user into the current security context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Create a token for that user
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User)
                authentication.getPrincipal();

        String jwt = tokenUtils.generateToken(user.getUsername(), user.getAuthorities().toString());
        int expiresIn = tokenUtils.getExpiredIn();

        return ResponseEntity.ok(new JwtDTO(jwt, expiresIn));

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(value = "/add_administrator")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<AdministratorDTO> addAdministrator(@RequestBody AdministratorDTO administratorDTO){
        User existUser = userServiceInterface.findOneByUsername(administratorDTO.getUsername());
        if (existUser != null) {
            return ResponseEntity.status(409).build();
        }
        return ResponseEntity.ok().body(administratorServiceInterface.save(administratorDTO));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(value = "/add_repairer")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<RepairerDTO> addRepairer(@RequestBody RepairerDTO repairerDTO){
        User existUser = userServiceInterface.findOneByUsername(repairerDTO.getUsername());
        if (existUser != null) {
            return ResponseEntity.status(409).build();
        }
        return ResponseEntity.ok().body(repairerServiceInterface.save(repairerDTO));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(value = "/add_customer")
    public ResponseEntity<CustomerDTO> addCustomer(@RequestBody CustomerDTO customerDTO){
        User existUser = userServiceInterface.findOneByUsername(customerDTO.getUsername());
        if (existUser != null) {
            return ResponseEntity.status(409).build();
        }
        return ResponseEntity.ok().body(customerServiceInterface.save(customerDTO));
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") Long id){
        userServiceInterface.remove(id);
        return  ResponseEntity.noContent().build();
    }

}
