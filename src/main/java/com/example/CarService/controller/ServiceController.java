package com.example.CarService.controller;

import com.example.CarService.dto.ServiceDTO;
import com.example.CarService.serviceInterface.ServiceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/service")
public class ServiceController {

    @Autowired
    ServiceServiceInterface serviceServiceInterface;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<List<ServiceDTO>> getServices(){
        return ResponseEntity.ok().body(serviceServiceInterface.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ServiceDTO> getService(@PathVariable("id") Long id){
        return ResponseEntity.ok().body(serviceServiceInterface.findOne(id));
    }

    @PostMapping(value = "/add_service")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<ServiceDTO> addService(@RequestBody ServiceDTO serviceDTO){
        return ResponseEntity.ok().body(serviceServiceInterface.save(serviceDTO));
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public ResponseEntity<Void> deleteService(@PathVariable("id") Long id){
        serviceServiceInterface.remove(id);
        return  ResponseEntity.noContent().build();
    }

}
