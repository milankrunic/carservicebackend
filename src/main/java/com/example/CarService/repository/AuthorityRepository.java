package com.example.CarService.repository;

import com.example.CarService.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority findByNameAuthority(String nameAuthority);

}
