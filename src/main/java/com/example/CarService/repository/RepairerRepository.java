package com.example.CarService.repository;

import com.example.CarService.model.Repairer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepairerRepository extends JpaRepository<Repairer, Long> {
}
