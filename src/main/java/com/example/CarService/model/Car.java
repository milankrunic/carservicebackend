package com.example.CarService.model;

import com.example.CarService.model.enums.Fuel;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_car", nullable = false, unique = true)
    private Long idCar;

    @Column(name = "year_of_production", nullable = true)
    private String yearOfProduction;

    @Column(name = "engine_power", nullable = true)
    private String enginePower;

    @Column(name = "engine_displacement", nullable = true)
    private String engineDisplacement;

    @Column(name = "fuel", nullable = false)
    @Enumerated(EnumType.STRING)
    private Fuel fuel;

    @ManyToOne
    @JoinColumn(name = "owner", referencedColumnName = "id_customer", nullable = false)
    private Customer owner;

    @ManyToOne
    @JoinColumn(name = "car_brand_model", referencedColumnName = "id_car_brand_model", nullable = false)
    private CarBrandModel carBrandModel;

    @OneToMany(mappedBy="car", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<ServiceBook> serviceBooks = new ArrayList<ServiceBook>();

}
