package com.example.CarService.model;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "service_book")
public class ServiceBook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_service_book", nullable = false, unique = true)
    private Long idServiceBook;

    @ManyToOne
    @JoinColumn(name = "car", referencedColumnName = "id_car", nullable = false)
    private Car car;

    @ManyToOne
    @JoinColumn(name = "service", referencedColumnName = "id_service", nullable = false)
    private Service service;

}
