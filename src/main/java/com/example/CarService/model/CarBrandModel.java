package com.example.CarService.model;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "car_brand_model")
public class CarBrandModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_car_brand_model", nullable = false, unique = true)
    private Long idCarBrandModel;

    @Column(name = "brand", nullable = true)
    private String brand;

    @Column(name = "model", nullable = true)
    private String model;

    @OneToMany(mappedBy="carBrandModel", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<Car> cars = new ArrayList<Car>();

    @OneToMany(mappedBy="carBrandModel", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<CarPart> carParts = new ArrayList<CarPart>();

}
