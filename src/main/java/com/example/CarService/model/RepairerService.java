package com.example.CarService.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "repairer_service")
public class RepairerService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_repairer_service", nullable = false, unique = true)
    private Long idRepairerService;

    @ManyToOne
    @JoinColumn(name = "repairer", referencedColumnName = "id_repairer", nullable = false)
    private Repairer repairer;

    @ManyToOne
    @JoinColumn(name = "service", referencedColumnName = "id_service", nullable = false)
    private Service service;

}
