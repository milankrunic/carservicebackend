package com.example.CarService.model.enums;

public enum Specialization {
    Auto_mechanic, Car_electrician, Vulcanizer, Tinsmith;
}
