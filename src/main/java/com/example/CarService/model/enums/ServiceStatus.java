package com.example.CarService.model.enums;

public enum ServiceStatus {
    Scheduled, Completed, Cancelled;
}
