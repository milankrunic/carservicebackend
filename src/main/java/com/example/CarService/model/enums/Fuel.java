package com.example.CarService.model.enums;

public enum Fuel {
    Gasoline, Diesel, Electric;
}
