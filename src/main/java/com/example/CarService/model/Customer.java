package com.example.CarService.model;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_customer", nullable = false, unique = true)
    private Long idCustomer;

    @Column(name = "winning_points", nullable = true)
    private double winningPoints;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id_user", nullable = false)
    private User user;

    @OneToMany(mappedBy="owner", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<Car> cars = new ArrayList<Car>();

}
