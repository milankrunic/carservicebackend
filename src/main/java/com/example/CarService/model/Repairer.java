package com.example.CarService.model;

import com.example.CarService.model.enums.Gender;
import com.example.CarService.model.enums.Specialization;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "repairer")
public class Repairer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_repairer", nullable = false, unique = true)
    private Long idRepairer;

    @Column(name = "salary", nullable = true)
    private double salary;

    @Column(name = "specialization", nullable = false)
    @Enumerated(EnumType.STRING)
    private Specialization specialization;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id_user", nullable = false)
    private User user;

    @OneToMany(mappedBy="repairer", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<RepairerService> repairerServices = new ArrayList<RepairerService>();

}
