package com.example.CarService.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_authority")
public class UserAuthority{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_authority", nullable = false, unique = true)
    private Long idUserAuthority;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id_user", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "authority_id", referencedColumnName = "id_authority", nullable = false)
    private Authority authority;

    public UserAuthority(User user, Authority authority) {
        super();
        this.user = user;
        this.authority=authority;
    }

}


