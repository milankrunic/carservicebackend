package com.example.CarService.model;

import com.example.CarService.model.enums.Gender;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user", nullable = false, unique = true)
    private Long idUser;

    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Column(name = "last_name", nullable = true)
    private String lastName;

    @Column(name = "gender", nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "address", nullable = true)
    private String address;

    @Column(name = "phone_number", nullable = true)
    private String phoneNumber;

    @Column(name = "username", nullable = true)
    private String username;

    @Column(name = "password", nullable = true)
    private String password;

    @OneToMany(mappedBy="user", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<Administrator> administrators = new ArrayList<Administrator>();

    @OneToMany(mappedBy="user", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<Customer> customers = new ArrayList<Customer>();

    @OneToMany(mappedBy="user", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<Repairer> repairers = new ArrayList<Repairer>();

    @OneToMany(mappedBy="user", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<UserAuthority> userAuthorities = new ArrayList<UserAuthority>();

}
