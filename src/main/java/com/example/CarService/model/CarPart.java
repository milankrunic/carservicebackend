package com.example.CarService.model;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "car_part")
public class CarPart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_car_part", nullable = false, unique = true)
    private Long idCarPart;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "price", nullable = true)
    private Double price;

    @ManyToOne
    @JoinColumn(name = "car_brand_model", referencedColumnName = "id_car_brand_model", nullable = false)
    private CarBrandModel carBrandModel;

    @ManyToOne
    @JoinColumn(name = "service", referencedColumnName = "id_service", nullable = false)
    private Service service;

}
