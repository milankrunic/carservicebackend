package com.example.CarService.model;

import com.example.CarService.model.enums.ServiceStatus;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "service")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_service", nullable = false, unique = true)
    private Long idService;

    @Column(name = "service_date", nullable = true)
    private Date serviceDate;

    @Column(name = "description", nullable = true)
    private String description;

    @Column(name = "service_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private ServiceStatus serviceStatus;

    @OneToMany(mappedBy="service", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<RepairerService> repairerServices = new ArrayList<RepairerService>();

    @OneToMany(mappedBy="service", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<CarPart> carParts = new ArrayList<CarPart>();

    @OneToMany(mappedBy="service", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<ServiceBook> serviceBooks = new ArrayList<ServiceBook>();

}
