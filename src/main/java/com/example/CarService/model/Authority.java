package com.example.CarService.model;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="authority")
public class Authority implements GrantedAuthority {

//    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_authority", nullable = false, unique = true)
    private Long idAuthority;

    @Column(name = "name_authority", nullable = false)
    private String nameAuthority;

    @OneToMany(mappedBy="authority", cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<UserAuthority> userAuthorities = new ArrayList<UserAuthority>();

    @Override
    public String getAuthority() {
        return nameAuthority;
    }

}


