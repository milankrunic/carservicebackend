package com.example.CarService.service;

import com.example.CarService.model.Authority;
import com.example.CarService.repository.AuthorityRepository;
import com.example.CarService.serviceInterface.AuthorityServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService implements AuthorityServiceInterface {

    @Autowired
    AuthorityRepository authorityRepository;

    @Override
    public Authority findByNameAuthority(String nameAuthority) {
        return authorityRepository.findByNameAuthority(nameAuthority);
    }
}
