package com.example.CarService.service;

import com.example.CarService.dto.RepairerDTO;
import com.example.CarService.model.*;
import com.example.CarService.repository.RepairerRepository;
import com.example.CarService.serviceInterface.AuthorityServiceInterface;
import com.example.CarService.serviceInterface.RepairerServiceInterface;
import com.example.CarService.serviceInterface.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RepairerService implements RepairerServiceInterface {

    @Autowired
    RepairerRepository repairerRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthorityServiceInterface authorityServiceInterface;

    @Autowired
    UserServiceInterface userServiceInterface;

    @Override
    public List<RepairerDTO> findAll() {
        List<Repairer> repairers = repairerRepository.findAll();

        List<RepairerDTO> repairerDTO = new ArrayList<>();
        for (Repairer repairer : repairers){
            repairerDTO.add(new RepairerDTO(repairer));
        }
        return repairerDTO;
    }

    @Override
    public RepairerDTO save(RepairerDTO repairerDTO) {
        User user = new User();

        user.setFirstName(repairerDTO.getFirstName());
        user.setLastName(repairerDTO.getLastName());
        user.setGender(repairerDTO.getGender());
        user.setAddress(repairerDTO.getAddress());
        user.setPhoneNumber(repairerDTO.getPhoneNumber());
        user.setUsername(repairerDTO.getUsername());
        user.setPassword(passwordEncoder.encode(repairerDTO.getPassword()));

        user = userServiceInterface.save(user);

        Authority authority = authorityServiceInterface.findByNameAuthority("ROLE_REPAIRER");
        UserAuthority userAuth = new UserAuthority(user, authority);
        user.getUserAuthorities().add(userAuth);

        Repairer repairer = new Repairer();

        repairer.setSalary(repairerDTO.getSalary());
        repairer.setSpecialization(repairerDTO.getSpecialization());
        repairer.setUser(user);

        repairer = repairerRepository.save(repairer);

        return new RepairerDTO(repairer);
    }
}
