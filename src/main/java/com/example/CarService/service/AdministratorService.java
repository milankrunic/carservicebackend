package com.example.CarService.service;

import com.example.CarService.dto.AdministratorDTO;
import com.example.CarService.model.Administrator;
import com.example.CarService.model.Authority;
import com.example.CarService.model.User;
import com.example.CarService.model.UserAuthority;
import com.example.CarService.repository.AdministratorRepository;
import com.example.CarService.serviceInterface.AdministratorServiceInterface;
import com.example.CarService.serviceInterface.AuthorityServiceInterface;
import com.example.CarService.serviceInterface.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdministratorService implements AdministratorServiceInterface {

    @Autowired
    AdministratorRepository administratorRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserServiceInterface userServiceInterface;

    @Autowired
    AuthorityServiceInterface authorityServiceInterface;

    @Override
    public List<AdministratorDTO> findAll() {
        List<Administrator> administrators = administratorRepository.findAll();

        List<AdministratorDTO> administratorDTO = new ArrayList<>();
        for (Administrator administrator : administrators){
            administratorDTO.add(new AdministratorDTO(administrator));
        }
        return administratorDTO;
    }

    @Override
    public AdministratorDTO save(AdministratorDTO administratorDTO) {
        User user = new User();

        user.setFirstName(administratorDTO.getFirstName());
        user.setLastName(administratorDTO.getLastName());
        user.setGender(administratorDTO.getGender());
        user.setAddress(administratorDTO.getAddress());
        user.setPhoneNumber(administratorDTO.getPhoneNumber());
        user.setUsername(administratorDTO.getUsername());
        user.setPassword(passwordEncoder.encode(administratorDTO.getPassword()));

        user = userServiceInterface.save(user);

        Authority authority = authorityServiceInterface.findByNameAuthority("ROLE_ADMINISTRATOR");
        UserAuthority userAuth = new UserAuthority(user, authority);
        user.getUserAuthorities().add(userAuth);

        Administrator administrator = new Administrator();

        administrator.setSalary(administratorDTO.getSalary());
        administrator.setUser(user);

        administrator = administratorRepository.save(administrator);

        return new AdministratorDTO(administrator);
    }
}
