package com.example.CarService.service;

import com.example.CarService.dto.UserDTO;
import com.example.CarService.model.User;
import com.example.CarService.repository.UserRepository;
import com.example.CarService.serviceInterface.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements UserServiceInterface {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public User findOneByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<UserDTO> findAll() {
        List<User> users = userRepository.findAll();

        List<UserDTO> userDTO = new ArrayList<>();
        for (User user : users){
            userDTO.add(new UserDTO(user));
        }
        return userDTO;
    }

    @Override
    public UserDTO findOne(Long id) {
        User user = userRepository.getOne(id);
        return new UserDTO(user);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void remove(Long id) {
        userRepository.deleteById(id);
    }


}
