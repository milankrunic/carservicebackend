package com.example.CarService.service;

import com.example.CarService.dto.ServiceDTO;
import com.example.CarService.repository.ServiceRepository;
import com.example.CarService.serviceInterface.ServiceServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceService implements ServiceServiceInterface {

    @Autowired
    ServiceRepository serviceRepository;

    @Override
    public List<ServiceDTO> findAll() {
        List<com.example.CarService.model.Service> services = serviceRepository.findAll();

        List<ServiceDTO> serviceDTO = new ArrayList<>();
        for (com.example.CarService.model.Service service : services){
            serviceDTO.add(new ServiceDTO(service));
        }
        return serviceDTO;
    }

    @Override
    public ServiceDTO findOne(Long id) {
        com.example.CarService.model.Service service = serviceRepository.getOne(id);
        return new ServiceDTO(service);
    }

    @Override
    public ServiceDTO save(ServiceDTO serviceDTO) {
        com.example.CarService.model.Service service = new com.example.CarService.model.Service();

        service.setServiceDate(serviceDTO.getServiceDate());
        service.setDescription(serviceDTO.getDescription());
        service.setServiceStatus(serviceDTO.getServiceStatus());

        service = serviceRepository.save(service);

        return new ServiceDTO(service);
    }

    @Override
    public void remove(Long id) {
        serviceRepository.deleteById(id);
    }
}
