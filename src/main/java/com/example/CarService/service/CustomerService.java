package com.example.CarService.service;

import com.example.CarService.dto.CustomerDTO;
import com.example.CarService.model.*;
import com.example.CarService.repository.CustomerRepository;
import com.example.CarService.serviceInterface.AuthorityServiceInterface;
import com.example.CarService.serviceInterface.CustomerServiceInterface;
import com.example.CarService.serviceInterface.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService implements CustomerServiceInterface {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserServiceInterface userServiceInterface;

    @Autowired
    AuthorityServiceInterface authorityServiceInterface;

    @Override
    public List<CustomerDTO> findAll() {
        List<Customer> customers = customerRepository.findAll();

        List<CustomerDTO> customerDTO = new ArrayList<>();
        for (Customer customer : customers){
            customerDTO.add(new CustomerDTO(customer));
        }
        return customerDTO;
    }

    @Override
    public CustomerDTO save(CustomerDTO customerDTO) {
        User user = new User();

        user.setFirstName(customerDTO.getFirstName());
        user.setLastName(customerDTO.getLastName());
        user.setGender(customerDTO.getGender());
        user.setAddress(customerDTO.getAddress());
        user.setPhoneNumber(customerDTO.getPhoneNumber());
        user.setUsername(customerDTO.getUsername());
        user.setPassword(passwordEncoder.encode(customerDTO.getPassword()));

        user = userServiceInterface.save(user);

        Authority authority = authorityServiceInterface.findByNameAuthority("ROLE_CUSTOMER");
        UserAuthority userAuth = new UserAuthority(user, authority);
        user.getUserAuthorities().add(userAuth);

        Customer customer = new Customer();

        customer.setWinningPoints(0.0);
        customer.setUser(user);

        customer = customerRepository.save(customer);

        return new CustomerDTO(customer);
    }
}
