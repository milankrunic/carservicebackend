--AUTHORITY
INSERT INTO authority(name_authority) VALUES('ROLE_ADMINISTRATOR');
INSERT INTO authority(name_authority) VALUES('ROLE_REPAIRER');
INSERT INTO authority(name_authority) VALUES('ROLE_CUSTOMER');

--USER
INSERT INTO user(first_name, last_name, gender, address, phone_number, username, password)
	VALUES('Milan', 'Krunic', 'Male', 'Cara Dusana 50', '063123456', 'milank', '$2a$12$U2.nVIzwjWSuxm/uetSKZeg.pcln8O46qjhlDBEb.qsruGVwpVH9i'); --milan123
INSERT INTO user(first_name, last_name, gender, address, phone_number, username, password)
	VALUES('Mika', 'Mikic', 'Male', 'Vojvodjanska 50', '064111222', 'mikam', '$2a$12$SAJR.L8airvoGBYfM2XIKu58S/vDujMeuUhkTf7IWCIe.vjItwCbW'); --mika123
INSERT INTO user(first_name, last_name, gender, address, phone_number, username, password)
	VALUES('Sima', 'Simic', 'Male', 'Kralja Petra 110', '060321654', 'simas', '$2a$12$CY8LfxIXXOTTfS4IpkrmHubZTbRKNujAEAlTprRwH4FCcoiKEkePa'); --sima123
INSERT INTO user(first_name, last_name, gender, address, phone_number, username, password)
	VALUES('Jovana', 'Jovic', 'Female', 'Ustanicka 10', '065123321', 'jovanaj', '$2a$12$GYJWOvvorjGNKKgJr2sJK.jJFz/mTC/sdvA0Hd3V6k2AuczYUe23y'); --jovana123
INSERT INTO user(first_name, last_name, gender, address, phone_number, username, password)
	VALUES('Jova', 'Jovic', 'Male', 'Ustanicka 10', '062222333', 'jovaj', '$2a$12$yrwshaUkhXJoFQsgPk10zueCQRSAIJWTN10p5OM9n2BasHMYKRg0C'); --jova123

--SERVICE
INSERT INTO service(service_date, description, service_status)
	VALUES('2023-12-10', 'Radio is broken', 'Scheduled');
INSERT INTO service(service_date, description, service_status)
	VALUES('2023-11-21', 'Engine needs to be repaired', 'Completed');

--ADMINISTRATOR
INSERT INTO administrator(salary, user_id)
	VALUES(105.000, 1);

--REPAIRER
INSERT INTO repairer(salary, specialization, user_id)
	VALUES(80.000, 'Auto_mechanic', 2);
INSERT INTO repairer(salary, specialization, user_id)
	VALUES(75.000, 'Car_electrician', 3);

--CUSTOMER
INSERT INTO customer(winning_points, user_id)
	VALUES(4.0, 4);
INSERT INTO customer(winning_points, user_id)
	VALUES(7.0, 5);

--USER AUTHORITY
INSERT INTO user_authority(user_id, authority_id) VALUES(1, 1);
INSERT INTO user_authority(user_id, authority_id) VALUES(2, 2);
INSERT INTO user_authority(user_id, authority_id) VALUES(3, 2);
INSERT INTO user_authority(user_id, authority_id) VALUES(4, 3);
INSERT INTO user_authority(user_id, authority_id) VALUES(5, 3);

--REPAIRER SERVICE
INSERT INTO repairer_service(repairer, service)
	VALUES(1, 2);
INSERT INTO repairer_service(repairer, service)
    VALUES(2, 1);
INSERT INTO repairer_service(repairer, service)
    VALUES(2, 2);